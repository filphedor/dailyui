"use strict";

var $ = require('jquery');
var mustache = require('mustache');

var type = function(input, message, callback) {
  if (message == '') {
    callback();
  } else {
    input.val(input.val() + message[0]);

    setTimeout(function() {
      type(input, message.substring(1, message.length + 1), callback);
    }, 250);
  }
}

var textBubble = function (from, to, message, time, callback) {
  var input = '.js-text';
  var container = '.js-message-container';

  var phoneFrom = $(from);
  var phoneTo = $(to);

  var textInput = phoneFrom.find(input);

  type(textInput, message, function() {
    var messageTemplate = $('#message').html();
    var fromMessage = { sideText: 'right', content: message, time: time};
    var toMessage = { sideText: 'left', content: message, time: time};

    var newFromMessage = $(mustache.render(messageTemplate, fromMessage));
    var newToMessage = $(mustache.render(messageTemplate, toMessage));

    var fromBubble = newFromMessage.find('.js-bubble');
    var toBubble = newToMessage.find('.js-bubble');

    textInput.val('');
    phoneFrom.find(container).append(newFromMessage);

    setTimeout(function() {
      phoneTo.find(container).append(newToMessage);
      toBubble.removeClass('out');
      callback();
    }, 1000);
  });
};

module.exports = {
  textBubble: textBubble
}