"use strict";

var $ = require('jquery');
var mustache = require('mustache');

var popUp = function() {
  var popUpTemplate = $('#pop-up-template').html();
  var popUpContent = mustache.render(popUpTemplate, {});
  var popUp = $(popUpContent);

  $('body').append(popUp);

  setTimeout(function() {
    popUp.removeClass('pop-up--closed');
    popUp.addClass('pop-up--open');
  }, 1);
};

module.exports = {
  popUp: popUp
};
