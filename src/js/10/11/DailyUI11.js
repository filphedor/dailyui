"use strict";

var $ = require('jquery');
var mustache = require('mustache');

var flash = function(color, text) {
  var template = $('#flash-template').html();
  var content = mustache.render(template, { color: color, text: text });
  var flashElement = $(content);

  $('body').append(flashElement);

  setTimeout(function() {
    flashElement.remove();
  }, 5000);
};

module.exports = {
  flash: flash
}
