"use strict";

var $ = require('jquery');
var countdown = require('./countdown');

module.exports = {
  $: $,
  countdown: countdown
}