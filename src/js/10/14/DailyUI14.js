"use strict";

var $ = require('jquery');

var zero  = require('./zero');
var one   = require('./one');
var two   = require('./two');
var three = require('./three');
var four  = require('./four');
var five  = require('./five');
var six   = require('./six');
var seven = require('./seven');
var eight = require('./eight');
var nine  = require('./nine');
var colon  = require('./colon');

var numSquares = 16;
var numColumns = 6;
var numRows = 8;

var minuteTen = '.js-minute-ten';
var minuteOne = '.js-minute-one';
var middle = '.js-colon';
var secondTen = '.js-second-ten';
var secondOne = '.js-second-one';
var square = '.js-square';

var numbers = [
  zero,
  one,
  two,
  three,
  four,
  five,
  six,
  seven,
  eight,
  nine,
  colon
];

var setUp = function() {
  var squareTemplate = $("#square-template").html();

  $('.js-number').each(function() {
    for (var i = 0; i < numSquares; i++) {
      $(this).append(squareTemplate);
    }
  });
};

var setToNumber = function(digit, number) {
  var digitElement = $(digit);

  var coords = numbers[number];

  digitElement.find(square).each(function(index) {
    var squareCoords = coords[index];

    //convert to percents
    var x = squareCoords.x / numColumns * 100;
    var y = squareCoords.y / numRows * 100;

    $(this).css({
      top: y + '%',
      left: x + '%'
    });
  });
};

var getNumberString = function(number) {
  if (number < 10) {
    return '0' + number.toString();
  } else {
    return number.toString();
  }
};

var updateTime = function(time) {
  var minutes = getNumberString(time.minutes);
  var seconds = getNumberString(time.seconds);

  setToNumber(minuteTen, Number.parseInt(minutes[0]));
  setToNumber(minuteOne, Number.parseInt(minutes[1]));

  setToNumber(secondTen, Number.parseInt(seconds[0]));
  setToNumber(secondOne, Number.parseInt(seconds[1]));

  if (!(minutes == '00' && seconds == '00')) {
    var newMinutes = time.minutes;
    var newSeconds = time.seconds;

    if (newSeconds == 0) {
      newSeconds = 59;
      newMinutes = newMinutes - 1;
    } else {
     newSeconds = newSeconds - 1;
    }
    var timeToChange = 1000 - new Date().getMilliseconds();

    setTimeout(function() {
      updateTime({minutes: newMinutes, seconds: newSeconds});
    }, timeToChange);
  }
};

var countdown = function(time) {
  var startMinutes = time.minutes;
  var startSeconds = time.seconds;

  setUp();

  //set colon
  setToNumber(middle, 10);

  updateTime(time);
}

module.exports = {
  countdown: countdown
}