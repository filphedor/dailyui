"use strict";

var $ = require('jquery');

var DailyUI02 = require('./00/02/DailyUI02');
var DailyUI03 = require('./00/03/DailyUI03');
var DailyUI04 = require('./00/04/DailyUI04');
var DailyUI06 = require('./00/06/DailyUI06');
var DailyUI07 = require('./00/07/DailyUI07');
var DailyUI08 = require('./00/08/DailyUI08');
var DailyUI09 = require('./00/09/DailyUI09');
var DailyUI11 = require('./10/11/DailyUI11');
var DailyUI13 = require('./10/13/DailyUI13');
var DailyUI14 = require('./10/14/DailyUI14');
var DailyUI16 = require('./10/16/DailyUI16');

module.exports = {
  $: $,
  DailyUI02: DailyUI02,
  DailyUI03: DailyUI03,
  DailyUI04: DailyUI04,
  DailyUI06: DailyUI06,
  DailyUI07: DailyUI07,
  DailyUI08: DailyUI08,
  DailyUI09: DailyUI09,
  DailyUI11: DailyUI11,
  DailyUI13: DailyUI13,
  DailyUI14: DailyUI14,
  DailyUI16: DailyUI16,
}
