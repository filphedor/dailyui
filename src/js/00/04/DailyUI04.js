"use strict";

var $ = require('jquery');
var mustache = require('mustache');

var topButtons = [
  {
    text: "Y=",
    second: "STAT PLOT",
    alpha: "F1"
  },
  {
    text: "WINDOW",
    second: "TBLSET",
    alpha: "F2"
  },
  {
    text: "ZOOM",
    second: "FORMAT",
    alpha: "F3"
  },
  {
    text: "TRACE",
    second: "CALC",
    alpha: "F4"
  },
  {
    text: "GRAPH",
    second: "TABLE",
    alpha: "F5"
  }
];

var firstButtons = [
  {
    "text": "2ND",
    "second": "",
    "alpha": "",
    "color": "#64B5F6",
    "textColor": "#FFFFFF"
  },
  {
    "text": "MODE",
    "second": "QUIT",
    "alpha": "",
    "color": "#616161",
    "textColor": "#FFFFFF"
  },
  {
    "text": "DEL",
    "second": "INS",
    "alpha": "",
    "color": "#616161",
    "textColor": "#FFFFFF"
  },
  {
    "text": "ALPHA",
    "second": "",
    "alpha": "",
    "color": "#81C784",
    "textColor": "#FFFFFF"
  },
  {
    "text": "X,T,O,n",
    "second": "",
    "alpha": "",
    "color": "#616161",
    "textColor": "#FFFFFF"
  },
  {
    "text": "STAT",
    "second": "",
    "alpha": "",
    "color": "#616161",
    "textColor": "#FFFFFF"
  }
];

var restButtons = [
  {
    "text": "MATH",
    "second": "TEST",
    "alpha": "A",
    "color": "#616161",
    "textColor": "#FFFFFF"
  },
  {
    "text": "APPS",
    "second": "ANGLE",
    "alpha": "B",
    "color": "#616161",
    "textColor": "#FFFFFF"
  },
  {
    "text": "PRGM",
    "second": "DRAW",
    "alpha": "C",
    "color": "#616161",
    "textColor": "#FFFFFF"
  },
  {
    "text": "VARS",
    "second": "DISTR",
    "alpha": "",
    "color": "#616161",
    "textColor": "#FFFFFF"
  },
  {
    "text": "CLEAR",
    "second": "",
    "alpha": "",
    "color": "#616161",
    "textColor": "#FFFFFF"
  },
  {
    "text": "X^-1",
    "second": "MATRIX",
    "alpha": "D",
    "color": "#616161",
    "textColor": "#FFFFFF"
  },
  {
    "text": "SIN",
    "second": "SIN-1",
    "alpha": "E",
    "color": "#616161",
    "textColor": "#FFFFFF"
  },
  {
    "text": "COS",
    "second": "COS-1",
    "alpha": "F",
    "color": "#616161",
    "textColor": "#FFFFFF"
  },
  {
    "text": "TAN",
    "second": "TAN-1",
    "alpha": "G",
    "color": "#616161",
    "textColor": "#FFFFFF"
  },
  {
    "text": "^",
    "second": "PI",
    "alpha": "H",
    "color": "#616161",
    "textColor": "#FFFFFF"
  },
  {
    "text": "x^2",
    "second": "SQRT",
    "alpha": "I",
    "color": "#616161",
    "textColor": "#FFFFFF"
  },
  {
    "text": ",",
    "second": "EE",
    "alpha": "J",
    "color": "#616161",
    "textColor": "#FFFFFF"
  },
  {
    "text": "(",
    "second": "{",
    "alpha": "K",
    "color": "#616161",
    "textColor": "#FFFFFF"
  },
  {
    "text": ")",
    "second": "}",
    "alpha": "L",
    "color": "#616161",
    "textColor": "#FFFFFF"
  },
  {
    "text": "÷",
    "second": "e",
    "alpha": "M",
    "color": "#EEEEEE",
    "textColor": "#FFFFFF"
  },
  {
    "text": "LOG",
    "second": "10^x",
    "alpha": "N",
    "color": "#616161",
    "textColor": "#FFFFFF"
  },
  {
    "text": "",
    "second": "",
    "alpha": "",
    "color": "",
    "textColor": ""
  },
  {
    "text": "",
    "second": "",
    "alpha": "",
    "color": "",
    "textColor": ""
  },
  {
    "text": "",
    "second": "",
    "alpha": "",
    "color": "",
    "textColor": ""
  },
  {
    "text": "X",
    "second": "[",
    "alpha": "R",
    "color": "#EEEEEE",
    "textColor": "#FFFFFF"
  },
  {
    "text": "LN",
    "second": "e^x",
    "alpha": "S",
    "color": "#616161",
    "textColor": "#FFFFFF"
  },
  {
    "text": "",
    "second": "",
    "alpha": "",
    "color": "",
    "textColor": ""
  },
  {
    "text": "",
    "second": "",
    "alpha": "",
    "color": "",
    "textColor": ""
  },
  {
    "text": "",
    "second": "",
    "alpha": "",
    "color": "",
    "textColor": ""
  },
  {
    "text": "-",
    "second": "]",
    "alpha": "W",
    "color": "#EEEEEE",
    "textColor": "#FFFFFF"
  },
  {
    "text": "STO>",
    "second": "RCL",
    "alpha": "X",
    "color": "#616161",
    "textColor": "#FFFFFF"
  },
  {
    "text": "",
    "second": "",
    "alpha": "",
    "color": "",
    "textColor": ""
  },
  {
    "text": "",
    "second": "",
    "alpha": "",
    "color": "",
    "textColor": ""
  },
  {
    "text": "",
    "second": "",
    "alpha": "",
    "color": "",
    "textColor": ""
  },
  {
    "text": "+",
    "second": "MEM",
    "alpha": "\"",
    "color": "#EEEEEE",
    "textColor": "#FFFFFF"
  }
];

var numberButtons = [
  {
    "text": "7",
    "second": "u",
    "alpha": "0"
  },
  {
    "text": "8",
    "second": "v",
    "alpha": "P"
  },
  {
    "text": "9",
    "second": "w",
    "alpha": "Q"
  },
  {
    "text": "4",
    "second": "L4",
    "alpha": "T"
  },
  {
    "text": "5",
    "second": "L%",
    "alpha": "U"
  },
  {
    "text": "6",
    "second": "L6",
    "alpha": "V"
  },
  {
    "text": "1",
    "second": "L!",
    "alpha": "Y"
  },
  {
    "text": "2",
    "second": "L2",
    "alpha": "Z"
  },
  {
    "text": "3",
    "second": "L3",
    "alpha": "O"
  },
  {
    "text": "0",
    "second": "CATALOG",
    "alpha": "_"
  },
  {
    "text": ".",
    "second": "i",
    "alpha": ":"
  },
  {
    "text": "(-)",
    "second": "ANS",
    "alpha": "?"
  }
];

var setUp = function() {
  var topButtonTemplate = $('#top-button-template').html();
  var firstButtonTemplate = $('#first-button-template').html();
  var restButtonTemplate = $('#rest-button-template').html();
  var numberButtonTemplate = $('#number-button-template').html();


  var topButtonContainer = $('.js-top-button-container');
  topButtons.forEach(function(button) {
    var buttonContent = mustache.render(topButtonTemplate, button);

    topButtonContainer.append(buttonContent);
  });

  var firstButtonContainer = $('.js-first-button-container');
  firstButtons.forEach(function(button) {
    var buttonContent = mustache.render(firstButtonTemplate, button);

    firstButtonContainer.append(buttonContent);
  });

  var restButtonContainer = $('.js-rest-button-container');
  restButtons.forEach(function(button) {
    var buttonContent = mustache.render(restButtonTemplate, button);

    restButtonContainer.append(buttonContent);
  });

  var numberButtonContainer = $('.js-number-button-container');
  numberButtons.forEach(function(button) {
    var buttonContent = mustache.render(numberButtonTemplate, button);

    numberButtonContainer.append(buttonContent);
  });
};

module.exports = {
  setUp: setUp
}
