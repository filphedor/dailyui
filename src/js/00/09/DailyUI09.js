"use strict";

var $ = require('jquery');
var mustache = require('mustache');

var songs = [
  {
    title: "Remember Me?",
    cover: "/img/09/kyle.jpeg",
    artist: "KYLE"
  },
  {
    title: "Run the World (Girls)",
    cover: "/img/09/beyonce.jpeg",
    artist: "Beyonce"
  },
  {
    title: "Angels",
    cover: "/img/09/last.jpeg",
    artist: "Chance the Rapper"
  },
  {
    title: "Starboy",
    cover: "/img/09/cover.jpeg",
    artist: "The Weeknd",
    selected: true
  },
  {
    title: "Trumpets",
    cover: "/img/09/next.jpeg",
    artist: "Jason Derulo"
  },
  {
    title: "Boredom",
    cover: "/img/09/tyler.jpeg",
    artist: "Tyler, the Creator"
  },

  {
    title: "Best Thing I Never Had",
    cover: "/img/09/beyonce.jpeg",
    artist: "Beyonce"
  },
  {
    title: "Stupid Love",
    cover: "/img/09/next.jpeg",
    artist: "Jason Derulo"
  },
  {
    title: "All Right",
    cover: "/img/09/kyle.jpeg",
    artist: "KYLE"
  }
];

var setUp = function() {
  var songTemplate = $('#song-template').html();
  var songContainer = $('.js-song-container');

  songs.forEach(function(song) {
    var songContent = mustache.render(songTemplate, song);

    songContainer.append(songContent);
  });
};

module.exports = {
  setUp: setUp
}
