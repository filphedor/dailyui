"use strict";

var $ = require('jquery');
var mustache = require('mustache');

var notifications = [
  {
    title: "Browser",
    prefix: "browser",
    subs: [
      {
        title: "All",
        suffix: "all"
      },
      {
        title: "New friend request",
        suffix: "new"
      },
      {
        title: "Likes",
        suffix: "likes"
      },
      {
        title: "Friend activity",
        suffix: "friend"
      }
    ]
  },
  {
    title: "Email",
    prefix: "email",
    subs: [
      {
        title: "All",
        suffix: "all"
      },
      {
        title: "New friend request",
        suffix: "new"
      },
      {
        title: "Likes",
        suffix: "likes"
      },
      {
        title: "Friend activity",
        suffix: "friend"
      }
    ]
  },
  {
    title: "Mobile",
    prefix: "mobile",
    subs: [
      {
        title: "All",
        suffix: "all"
      },
      {
        title: "New friend request",
        suffix: "new"
      },
      {
        title: "Likes",
        suffix: "likes"
      },
      {
        title: "Friend activity",
        suffix: "friend"
      }
    ]
  }
];

var setUp = function() {
  var notificationContainer = $('.js-notification-container');
  var notificationSectionTemplate = $('#notification-section-template').html();
  var notificationTemplate = $('#notification-template').html();
  var partials = {
    'notification-template': notificationTemplate
  };

  notifications.forEach(function(notification) {
    var notificationContent = mustache.render(
      notificationSectionTemplate,
      notification,
      partials
    );

    notificationContainer.append(notificationContent);
  });
};

module.exports = {
  setUp: setUp
}
