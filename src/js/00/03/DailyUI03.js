"use strict";

var $ = require('jquery');
var mustache = require('mustache');

var cards = [
    {
      items: [
        {
          text: "Clothing",
          icon: "fas fa-tshirt"
        },
        {
          text: "Books on Magic",
          icon: "fas fa-book"
        },
        {
          text: "Trick Cups",
          icon: "fas fa-coffee"
        }
      ]
    },
    {
      items: [
        {
          text: "Trick Dice",
          icon: "fas fa-dice"
        },
        {
          text: "Doves",
          icon: "fas fa-dove"
        },
        {
          text: "Pyrotechnics",
          icon: "fas fa-fire"
        }
      ]
    },
    {
      items: [
        {
          text: "Links and Rings",
          icon: "fas fa-link"
        },
        {
          text: "Wands",
          icon: "fas fa-magic"
        },
        {
          text: "Fake Currency",
          icon: "fas fa-dollar-sign"
        }
    ]
  }
];

var setUp = function() {
  var cardTemplate = $('#card-template').html();
  var cardContainer = $('.js-card-container');

  cards.forEach(function(card) {
    var cardContent = mustache.render(cardTemplate, card);

    cardContainer.append(cardContent);
  });
};

module.exports = {
  setUp: setUp
};