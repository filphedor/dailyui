"use strict";

var $ = require('jquery');
var mustache = require('mustache');

var scene = {
  graves: [
    {
      content: 4
    },
    {
      content: 0
    },
    {
      content: 4
    }
  ]
};

var setUp = function() {
  var sceneTemplate = $('#scene-template').html();
  var sceneContainer = $('.js-scene-container');

  var eyesTemplate = $('#eyes-template').html();
  var graveTemplate = $('#grave-template').html();
  var pumpkinTemplate = $('#pumpkin-template').html();
  var treeTemplate = $('#tree-template').html();

  var partials = {
    'eyes-template': eyesTemplate,
    'grave-template': graveTemplate,
    'pumpkin-template': pumpkinTemplate,
    'tree-template': treeTemplate
  };

  var sceneContent = mustache.render(sceneTemplate, scene, partials);

  sceneContainer.append(sceneContent);
};

module.exports = {
  setUp: setUp
}
