"use strict";

var $ = require('jquery');
var mustache = require('mustache');

var works = [
  {
    title: "Hamlet",
    icon: "skull",
    color: "black"
  },
  {
    title: "Romeo and Juliet",
    icon: "heart",
    color: "red"
  },
  {
    title: "Macbeth",
    icon: "crown",
    color: "yellow"
  },
  {
    title: "The Tempest",
    color: "blue"
  },
  {
    title: "Othello",
    color: "black"
  },
  {
    title: "Julius Caesar",
    color: "red"
  }
];

var setUp = function() {
  var workTemplate = $('#work-template').html();
  var workContainer = $('.js-work-container');

  works.forEach(function(work) {
    var workContent = mustache.render(workTemplate, work);

    workContainer.append(workContent);
  });
};

module.exports = {
  setUp: setUp
}
