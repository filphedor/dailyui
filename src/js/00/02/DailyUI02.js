"use strict";

var $ = require('jquery');
var mustache = require('mustache');

var creditCardSetUp = require('./creditCardSetUp');

var numPhones = 15;

var colors = [
  {
    color: 'red'
  },
  {
    color: 'blue'
  },
  {
    color: 'green'
  },
  {
    color: 'purple'
  }
];

var setUp = function() {
  var phoneTemplate = $('#phone-template').html();
  var creditCardTemplate = $('#credit-card-template').html();

  var partials = {
    'credit-card-template': creditCardTemplate
  };

  var data = {
    colors: colors
  };

  var phoneContent = mustache.render(phoneTemplate, data, partials);

  for (var i = 0; i < numPhones; i++) {
    $('.js-container').append(phoneContent);
  };
};

module.exports = {
  setUp: setUp,
  creditCardSetUp: creditCardSetUp
}
