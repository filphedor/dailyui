"use strict";

var $ = require('jquery');

var zSort = function(a, b) {
  return $(a).css('zIndex') - $(b).css('zIndex');
};

var appear = function(object) {
  $(object).addClass('visible');
};

//has to macth value in sass
var animationLength = 1000;

var setUp = function(cards, callback) {
  setTimeout(function() {
    cards.each(function() {
      appear(this);
    });

    setTimeout(function() {
      spread(cards);
    }, animationLength)
  }, animationLength);
};

var spread = function(cards) {
  var sortedCards = cards.sort(zSort);

  var spreadClasses = [
    'spread--1',
    'spread--2',
    'spread--3',
    'spread--4',
  ];

  for (var i = 0; i < sortedCards.length; i++) {
    var currCard = $(sortedCards[i]);

    currCard.addClass(spreadClasses[i]);
  }

  setTimeout(function() {
    for (var i = 0; i < cards.length; i++) {
      var currCard = $(sortedCards[i]);

      currCard.removeClass(spreadClasses[i]);
    }

    setTimeout(function() {
      shuffle(cards, 0);
    }, animationLength * 1.5);
  }, animationLength * 2);
};

var shuffle = function(cards, index) {
  if (index >= cards.length) {
    spread(cards);
  } else {
    if (index == 0) {
      var cards = cards.sort(zSort);
    }

    var currCard = $(cards[index]);

    currCard.addClass('shuffled');

    var newZ = index - 3;

    if (newZ < 0) {
      newZ = 5 + newZ;
    }

    setTimeout(function() {
      currCard.removeClass('shuffled');
      currCard.css('z-index', newZ);

      setTimeout(function() {
        shuffle(cards, index + 1);
      }, animationLength / 2);
    }, animationLength / 2);
  }
}

module.exports = function(cards) {
  setUp(cards);
};

